#!/usr/bin/bash
 # create directory structure for ansible
  sudo yum-config-manager --enable epel
  sudo yum -y install ansible
  sudo yum -y install gcc
  sudo yum -y install python-devel krb5-devel krb5-libs krb5-workstation
  sudo yum install -y python27-python-pip
  sudo scl enable python27 bash
  sudo pip install --upgrade pip
  sudo yum -y install tree
  sudo pip install pyOpenSSL
  sudo pip install pywinrm --ignore-installed requests
  sudo pip install pywinrm[kerberos]
  sudo pip install pywinrm[credssp] --ignore-installed pyOpenSSL
  sudo pip install boto3
  sudo pip install boto
  sudo pip install --upgrade pywinrm
  ansible --version
  sudo yum install -y git
  sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
  sudo sh -c 'echo -e "[azure-cli]\nname=Azure CLI\nbaseurl=https://packages.microsoft.com/yumrepos/azure-cli\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/azure-cli.repo'
  sudo yum install azure-cli